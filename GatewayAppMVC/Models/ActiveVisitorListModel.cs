﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayAppMVC.Models
{
    public class ActiveVisitorListModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string BeaconName { get; set; }
        public List<string> BannedGatewayList { get; set; }
        public List<string> AllowedGatewayList { get; set; }
    }
}