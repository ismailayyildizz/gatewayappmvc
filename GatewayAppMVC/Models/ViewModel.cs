﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayAppMVC.Models
{
    public class ViewModel
    {
        //Firts beacon
        public string CentroX1 { get; set; }
        public string CentroY1 { get; set; }
        public string Visitor1 { get; set; }
        //Second beacon
        public string CentroX2 { get; set; }
        public string CentroY2 { get; set; }
        public string Visitor2 { get; set; }
        //Third beacon
        public string CentroX3 { get; set; }
        public string CentroY3 { get; set; }
        public string Visitor3 { get; set; }
        //Fourth beacon
        public string CentroX4 { get; set; }
        public string CentroY4 { get; set; }
        public string Visitor4 { get; set; }
        //Fifth beacon
        public string CentroX5 { get; set; }
        public string CentroY5 { get; set; }
        public string Visitor5 { get; set; }
    }
}