﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayAppMVC.Models
{
    public class GetPositonsModel
    {
        public double CentroX; //x,y koordinat bilgileri
        public double CentroY; //x,y koordinat bilgileri
        public string UidBeacon;
        public int IdAsset;
        public string Classe { get; set; }
        public string Inventario { get; set; }//beacon adı
        public string Batteria { get; set; }
        public double TxPower1m { get; set; }
        public string InfoAggiuntive { get; set; }
        public UnitaServizio UnitaServizio { get; set; }

    }
    public class UnitaServizio
    {
        public int Id { get; set; }
        public string DataAggiornamento { get; set; }
        public DateTime DataCreazione { get; set; }
        public string Descrizione { get; set; }
        public string LayerName { get; set; }
        public string PathLoss { get; set; }
        public Blocchi Blocchi { get; set; }
        public Reparto Reparto { get; set; }
    }
    public class Blocchi {
        public int Id { get; set; }
        public int DataAggiornamento { get; set; }
        public DateTime DataCreazione { get; set; }
        public string Descrizione { get; set; }
        public Strutture Strutture { get; set; }

    }
    public class Strutture {
        public int Id { get; set; }
        public string Descrizione { get; set; }
        public string DataAggiornamento { get; set; }
        public DateTime DataCreazione { get; set; }
        public string WorkspaceName { get; set; }
    }
    public class Reparto {
        public int Id { get; set; }
        public string Nome_reparto { get; set; }
        public string Descrizione_reparto { get; set; }
    }

}