﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayAppMVC.Models
{
    public class BannedGateway
    {
        //HermanBeacon beacon'ıno anki sinyal verdigi yasaklı gateway'i
        public string GatewayName1 { get; set; }
        public bool IsBanned1 { get; set; }
        public string VisitorFullName1 { get; set; }
        //BillRed beacon'ın o anki sinyal verdigi yasaklı gateway'i
        public string GatewayName2 { get; set; }
        public bool IsBanned2 { get; set; }
        public string VisitorFullName2 { get; set; }
        //Agela Orange beacon'ın o anki sinyal verdigi yasaklı gateway'i
        public string GatewayName3 { get; set; }
        public bool IsBanned3 { get; set; }
        public string VisitorFullName3 { get; set; }
        //Suzan White beacon'ın o anki sinyal verdigi yasaklı gateway'i
        public string GatewayName4 { get; set; }
        public bool IsBanned4 { get; set; }
        public string VisitorFullName4 { get; set; }

    }
}