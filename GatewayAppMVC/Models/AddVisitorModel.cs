﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayAppMVC.Models
{
    public class AddVisitorModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int BeaconId { get; set; }
        public int Gateway1 { get; set; }
        public int Gateway2 { get; set; }
        public int Gateway3 { get; set; }
        public int Gateway4 { get; set; }
    }
}