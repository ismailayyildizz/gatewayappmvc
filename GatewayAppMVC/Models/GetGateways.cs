﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GatewayAppMVC.Models
{
    public class GetGateways
    {
        public ID id { get; set; }
        public string echodes { get; set; }

        public string echogeom { get; set; }

        public string exhoX { get; set; }
        public string echoY { get; set; }

        public long powerBeacon { get; set; }

        public int idAsset { get; set; }

        public string classe { get; set; }

        public string intertario { get; set; }

        public int txPower1m { get; set; }

        public int txPower0m { get; set; }

        public string struttura { get; set; }

        public string blocco { get; set; }

        public string unita { get; set; }

        public int idUnita { get; set; }

        public double pathLoss { get; set; }

        public string batteria { get; set; }
    }

    public class ID
    {
        public string uidBeacon { get; set; }

        public string uidEchobeacon { get; set; }
    }
}
