﻿using GatewayAppMVC.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GatewayAppMVC.Controllers
{
    public class WatchController : Controller
    {
        GatewayAppEntities db = new GatewayAppEntities();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetBeaconPosition()
         {
            var model =new BannedGateway();
            model.GatewayName1 = GetBannedGatewayHermanGreen().GatewayName1;
            model.VisitorFullName1 = GetBannedGatewayHermanGreen().VisitorFullName1;
            model.IsBanned1 = GetBannedGatewayHermanGreen().IsBanned1;

            model.GatewayName2 = GetBannedGatewayBillRed().GatewayName2;
            model.VisitorFullName2 = GetBannedGatewayBillRed().VisitorFullName2;
            model.IsBanned2 = GetBannedGatewayBillRed().IsBanned2;

            model.GatewayName3 = GetBannedGatewayAnitaOrange().GatewayName3;
            model.VisitorFullName3 = GetBannedGatewayAnitaOrange().VisitorFullName3;
            model.IsBanned3 = GetBannedGatewayAnitaOrange().IsBanned3;

            model.GatewayName4 = GetBannedGatewaySuzanWhite().GatewayName4;
            model.VisitorFullName4 = GetBannedGatewaySuzanWhite().VisitorFullName4;
            model.IsBanned4 = GetBannedGatewaySuzanWhite().IsBanned4;


            var result =new  {
                gatewayName1 = model.GatewayName1,
                visitorFullName1 = model.VisitorFullName1,
                isBanned1 = model.IsBanned1,

                gatewayName2 = model.GatewayName2,
                visitorFullName2 = model.VisitorFullName2,
                isBanned2 = model.IsBanned2,

                gatewayName3 = model.GatewayName3,
                visitorFullName3 = model.VisitorFullName3,
                isBanned3 = model.IsBanned3,

                gatewayName4 = model.GatewayName4,
                visitorFullName4 = model.VisitorFullName4,
                isBanned4 = model.IsBanned4
            };

            return Json(result, JsonRequestBehavior.AllowGet);

           // return PartialView("~/Views/Watch/Gateway1_PartialView.cshtml",model);
        }




        public BannedGateway GetBannedGatewayHermanGreen()
        {
            var bannedGatewayModel = new BannedGateway();

            var getGateways = GetGateways("Herman Green");
            if (getGateways != null && getGateways.Count() > 0) 
            {
                //VisitorGateway'dan 
                var visitorBeacon = db.VisitorBeacons.Where(x => x.BeaconID == 1 && x.IsDeleted == false).FirstOrDefault();
                if (visitorBeacon != null)
                {
                    //misafirin izinsiz alanları
                    var visitorGateway = db.VisitorGateways.Where(x => x.VisitorID == visitorBeacon.VisitorID && x.IsAllowed == false && x.IsDeleted==false).ToList();

                    var visitor = db.Visitors.Find(visitorBeacon.VisitorID);

                    if (visitorGateway.Count > 0)
                    {
                        //izinsiz gateway isimleri listesi
                        var bannedList = new List<string>();
                        foreach (var item in visitorGateway)
                        {
                            var banned = db.Gateways.Find(item.GatewayID);
                            bannedList.Add(banned.Name);
                        }
                        var list = new List<GetGateways>();

                        foreach (var b in bannedList)
                        {
                            //yakından sinyal aldığı gatewayler içinde yasaklı olanı içeriyor mu
                            var bannedGateway = getGateways.Where(x => x.echodes.Contains(b)).FirstOrDefault();

                            if (bannedGateway != null)
                            {
                                list.Add(bannedGateway);
                            }
                        }
                        //yasaklı bölgeler varsa
                        if (list != null)
                        {
                            //yasaklı bölgelerden hangisinden en yüksek sinyali alıyor
                            var nearGateway = list.FirstOrDefault(x => x.powerBeacon == list.Max(t => t.powerBeacon));

                            if (nearGateway!=null &&  nearGateway.powerBeacon >= -45)
                            {
                                //Yasaklı bölgeye girmeye çalışmış
                                bannedGatewayModel.IsBanned1 = true;
                                bannedGatewayModel.GatewayName1 = nearGateway.echodes;
                                bannedGatewayModel.VisitorFullName1 = visitor.FullName;
                            }
                            else
                            {
                                //yasaklı bölgeden uzaklaşmış
                                bannedGatewayModel.IsBanned1 = false;
                            }
                            //bannedGatewayModel.GatewayName = nearGateway.echodes;
                            //bannedGatewayModel.VisitorFullName = visitor.FullName;
                        }
                    }
                }
            }

            return bannedGatewayModel;
        }

        //Suzan White'in yasaklı oldugu bölgelerden hangisine en yakınsa onun bilgilerini döndürüyor.Yasaklı Bölgede degilse isBanned=false dönüyor sadece
        public BannedGateway GetBannedGatewaySuzanWhite()
        {
            var bannedGatewayModel = new BannedGateway();

            var getGateways = GetGateways("Susan White");
            if (getGateways != null && getGateways.Count() > 0)
            {
                //VisitorGateway'dan 
                var visitorBeacon = db.VisitorBeacons.Where(x => x.BeaconID == 3 && x.IsDeleted == false).FirstOrDefault();
                if (visitorBeacon != null)
                {
                    //misafirin izinsiz alanları
                    var visitorGateway = db.VisitorGateways.Where(x => x.VisitorID == visitorBeacon.VisitorID && x.IsAllowed == false && x.IsDeleted == false).ToList();

                    var visitor = db.Visitors.Find(visitorBeacon.VisitorID);

                    if (visitorGateway.Count > 0)
                    {
                        //izinsiz gateway isimleri listesi
                        var bannedList = new List<string>();
                        foreach (var item in visitorGateway)
                        {
                            var banned = db.Gateways.Find(item.GatewayID);
                            bannedList.Add(banned.Name);
                        }
                        var list = new List<GetGateways>();

                        foreach (var b in bannedList)
                        {
                            //yakından sinyal aldığı gatewayler içinde yasaklı olanı içeriyor mu
                            var bannedGateway = getGateways.Where(x => x.echodes.Contains(b)).FirstOrDefault();

                            if (bannedGateway != null)
                            {
                                list.Add(bannedGateway);
                            }
                        }
                        //yasaklı bölgeler varsa
                        if (list != null)
                        {
                            //yasaklı bölgelerden hangisinden en yüksek sinyali alıyor
                            var nearGateway = list.FirstOrDefault(x => x.powerBeacon == list.Max(t => t.powerBeacon));

                            if (nearGateway != null && nearGateway.powerBeacon >= -45)
                            {
                                //Yasaklı bölgeye girmeye çalışmış
                                bannedGatewayModel.IsBanned4 = true;
                                bannedGatewayModel.GatewayName4 = nearGateway.echodes;
                                bannedGatewayModel.VisitorFullName4 = visitor.FullName;
                            }
                            else
                            {
                                //yasaklı bölgeden uzaklaşmış
                                bannedGatewayModel.IsBanned4 = false;
                            }
                        }
                    }
                }
            }

            return bannedGatewayModel;
        }

        //Bill Red'in yasaklı oldugu bölgelerden hangisine en yakınsa onun bilgilerini döndürüyor.Yasaklı Bölgede degilse isBanned=false dönüyor sadece
        public BannedGateway GetBannedGatewayBillRed()
        {
            var bannedGatewayModel = new BannedGateway();

            var getGateways = GetGateways("Bill Red");
            if (getGateways != null && getGateways.Count() > 0)
            {
                //VisitorGateway'dan 
                var visitorBeacon = db.VisitorBeacons.Where(x => x.BeaconID == 4 && x.IsDeleted == false).FirstOrDefault();
                if (visitorBeacon != null)
                {
                    //misafirin izinsiz alanları
                    var visitorGateway = db.VisitorGateways.Where(x => x.VisitorID == visitorBeacon.VisitorID && x.IsAllowed == false && x.IsDeleted == false).ToList();

                    var visitor = db.Visitors.Find(visitorBeacon.VisitorID);

                    if (visitorGateway.Count > 0)
                    {
                        //izinsiz gateway isimleri listesi
                        var bannedList = new List<string>();
                        foreach (var item in visitorGateway)
                        {
                            var banned = db.Gateways.Find(item.GatewayID);
                            bannedList.Add(banned.Name);
                        }
                        var list = new List<GetGateways>();

                        foreach (var b in bannedList)
                        {
                            //yakından sinyal aldığı gatewayler içinde yasaklı olanı içeriyor mu
                            var bannedGateway = getGateways.Where(x => x.echodes.Contains(b)).FirstOrDefault();

                            if (bannedGateway != null)
                            {
                                list.Add(bannedGateway);
                            }
                        }
                        //yasaklı bölgeler varsa
                        if (list != null)
                        {
                            //yasaklı bölgelerden hangisinden en yüksek sinyali alıyor
                            var nearGateway = list.FirstOrDefault(x => x.powerBeacon == list.Max(t => t.powerBeacon));

                            if (nearGateway != null && nearGateway.powerBeacon >= -45)
                            {
                                //Yasaklı bölgeye girmeye çalışmış
                                bannedGatewayModel.IsBanned2 = true;
                                bannedGatewayModel.GatewayName2 = nearGateway.echodes;
                                bannedGatewayModel.VisitorFullName2 = visitor.FullName;
                            }
                            else
                            {
                                //yasaklı bölgeden uzaklaşmış
                                bannedGatewayModel.IsBanned2 = false;
                            }
                            //bannedGatewayModel.GatewayName = nearGateway.echodes;
                            //bannedGatewayModel.VisitorFullName = visitor.FullName;
                        }
                    }
                }
            }

            return bannedGatewayModel;
        }

        //Anita Orange'in yasaklı oldugu bölgelerden hangisine en yakınsa onun bilgilerini döndürüyor.Yasaklı Bölgede degilse isBanned=false dönüyor sadece
        public BannedGateway GetBannedGatewayAnitaOrange()
        {
            var bannedGatewayModel = new BannedGateway();

            var getGateways = GetGateways("Anita Orange");
            if (getGateways != null && getGateways.Count() > 0)
            {
                //VisitorGateway'dan 
                var visitorBeacon = db.VisitorBeacons.Where(x => x.BeaconID == 2 && x.IsDeleted == false).FirstOrDefault();
                if (visitorBeacon != null)
                {
                    //misafirin izinsiz alanları
                    var visitorGateway = db.VisitorGateways.Where(x => x.VisitorID == visitorBeacon.VisitorID && x.IsAllowed == false && x.IsDeleted == false).ToList();

                    var visitor = db.Visitors.Find(visitorBeacon.VisitorID);

                    if (visitorGateway.Count > 0)
                    {
                        //izinsiz gateway isimleri listesi
                        var bannedList = new List<string>();
                        foreach (var item in visitorGateway)
                        {
                            var banned = db.Gateways.Find(item.GatewayID);
                            bannedList.Add(banned.Name);
                        }
                        var list = new List<GetGateways>();

                        foreach (var b in bannedList)
                        {
                            //yakından sinyal aldığı gatewayler içinde yasaklı olanı içeriyor mu
                            var bannedGateway = getGateways.Where(x => x.echodes.Contains(b)).FirstOrDefault();

                            if (bannedGateway != null)
                            {
                                list.Add(bannedGateway);
                            }
                        }
                        //yasaklı bölgeler varsa
                        if (list != null)
                        {
                            //yasaklı bölgelerden hangisinden en yüksek sinyali alıyor
                            var nearGateway = list.FirstOrDefault(x => x.powerBeacon == list.Max(t => t.powerBeacon));

                            if (nearGateway != null && nearGateway.powerBeacon >= -45)
                            {
                                //Yasaklı bölgeye girmeye çalışmış
                                bannedGatewayModel.IsBanned3= true;
                                bannedGatewayModel.GatewayName3 = nearGateway.echodes;
                                bannedGatewayModel.VisitorFullName3 = visitor.FullName;
                            }
                            else
                            {
                                //yasaklı bölgeden uzaklaşmış
                                bannedGatewayModel.IsBanned3 = false;
                            }
                        }
                    }
                }
            }

            return bannedGatewayModel;
        }


        //Beacon'in sinyal aldıgı gatewaylerin bilgilerini dönüyor
        public List<GetGateways> GetGateways(string beaconName)
        {
            //string beaconName = "Herman Green";
            var client = new RestClient($"http://farmacode.bluesystem.it/bleRest/vBeaconRssiReceived/inventario={beaconName}");

            var request = new RestRequest(Method.GET);

            request.AddHeader("Content-Type", "application/json");

            request.AddParameter("Authorization", "Bearer " + "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsIm5vbWUiOiJpdmFuIiwiY29nbm9tZSI6IkNpcmxvIiwiaWQiOjEsInJvbGUiOiJBZG1pbmlzdHJhdG9yIiwiYWN0aXZlIjp0cnVlLCJpc3MiOiJodHRwOi8vaXQuYmx1ZXN5c3RlbS5jb20iLCJpYXQiOjE1OTI0NjIxOTgsImV4cCI6MTY4MjQ2MjE5OH0.QD-XpkceYMf8DbzXBeiKNzxZei60rSIEpS3hK1lxSJS6K3L7ynrpY8nMF9gmts_90_8U8EIORhp6LB2US7EOgw", ParameterType.HttpHeader);

            IRestResponse response = client.Execute(request);

            IRestResponse<List<GetGateways>> res = client.Get<List<GetGateways>>(request);

            if (response.IsSuccessful)
            {
                return res.Data;

            }
            else
            {
                return  new List<GetGateways>();
            }

        }

    }
}