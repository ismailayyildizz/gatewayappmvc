﻿using GatewayAppMVC.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GatewayAppMVC.Controllers
{
    public class MapController : Controller
    {
        GatewayAppEntities db = new GatewayAppEntities();
        public ActionResult Index()
        {
            return View();
        }

        //Haritada kullanıcıya atanmış aktif beacon takini
        public ActionResult GetPositonsView()
        {
            var list = GetPositions();
            var model = new ViewModel();
            var findVisitor = new VisitorBeacon();
            var findBeacon = new Beacon();

            if (list.Count >= 1)
            {
                var beaconName_1 = (list[0].Inventario).ToString();

                findBeacon = db.Beacons.Where(x => x.Name.Contains(beaconName_1)).FirstOrDefault();

                findVisitor = db.VisitorBeacons.Where(x => x.BeaconID==findBeacon.Id && x.IsDeleted == false).Include(x => x.Visitor).FirstOrDefault();

                if (findVisitor != null)
                {
                    model.Visitor1 = findVisitor.Visitor.FullName;

                    model.CentroX1 = (Math.Round(list[0].CentroX) * 10) + "px";
                    model.CentroY1 = (Math.Round(list[0].CentroY) * 10) + "px";
                }

            }
            if (list.Count >= 2)
            {
                var beaconName_2 = (list[1].Inventario).ToString();

                findBeacon = db.Beacons.Where(x => x.Name.Contains(beaconName_2)).FirstOrDefault();

                findVisitor = db.VisitorBeacons.Where(x => x.BeaconID== findBeacon.Id && x.IsDeleted == false).Include(x => x.Visitor).FirstOrDefault();

                if (findVisitor != null)
                {
                    model.Visitor2 = findVisitor.Visitor.FullName;

                    model.CentroX2 = (Math.Round(list[1].CentroX) * 10) + "px";
                    model.CentroY2 = (Math.Round(list[1].CentroY) * 10) + "px";
                }
            }
            if (list.Count >= 3)
            {
                var beaconName_3 = (list[2].Inventario).ToString();
                findBeacon = db.Beacons.Where(x => x.Name.Contains(beaconName_3)).FirstOrDefault();

                findVisitor = db.VisitorBeacons.Where(x =>x.BeaconID==findBeacon.Id && x.IsDeleted == false).Include(x => x.Visitor).FirstOrDefault();

                if (findVisitor != null)
                {
                    model.Visitor3 = findVisitor.Visitor.FullName;
                    model.CentroX3 = (Math.Round(list[2].CentroX) * 10) + "px";
                    model.CentroY3 = (Math.Round(list[2].CentroY) * 10) + "px";
                }
            }
            if (list.Count >= 4)
            {
                var beaconName_4 = (list[3].Inventario).ToString();
                findBeacon = db.Beacons.Where(x => x.Name.Contains(beaconName_4)).FirstOrDefault();

                findVisitor = db.VisitorBeacons.Where(x => x.BeaconID==findBeacon.Id && x.IsDeleted == false).Include(x => x.Visitor).FirstOrDefault();

                if (findVisitor != null)
                {
                    model.Visitor4 = findVisitor.Visitor.FullName;
                    model.CentroX4 = (Math.Round(list[3].CentroX) * 10) + "px";
                    model.CentroY4 = (Math.Round(list[3].CentroY) * 10) + "px";
                }
            }
            if (list.Count >= 5)
            {
                var beaconName_5 = (list[4].Inventario).ToString();
                findBeacon = db.Beacons.Where(x => x.Name.Contains(beaconName_5)).FirstOrDefault();
                findVisitor = db.VisitorBeacons.Where(x => x.BeaconID==findBeacon.Id && x.IsDeleted == false).Include(x => x.Visitor).FirstOrDefault();

                if (findVisitor != null)
                {
                    model.Visitor5 = findVisitor.Visitor.FullName;
                    model.CentroX5 = (Math.Round(list[4].CentroX) * 10) + "px";
                    model.CentroY5 = (Math.Round(list[4].CentroY) * 10) + "px";
                }
            }
            return PartialView("~/Views/Map/ChangePin_PartialView.cshtml", model);

        }

        //Aktif beaconların listesi
        public IList<GetPositonsModel> GetPositions()
        {
            var datas = new List<GetPositonsModel>();

            var client = new RestClient($"http://farmacode.bluesystem.it/bleRest/beaconPositionBarycenter/getPositions/struttura=Building1/blocco=Sector%201/unita=Unit%201");

            var request = new RestRequest(Method.GET);

            request.AddHeader("Content-Type", "application/json");

            request.AddParameter("Authorization", "Bearer " + "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsIm5vbWUiOiJpdmFuIiwiY29nbm9tZSI6IkNpcmxvIiwiaWQiOjEsInJvbGUiOiJBZG1pbmlzdHJhdG9yIiwiYWN0aXZlIjp0cnVlLCJpc3MiOiJodHRwOi8vaXQuYmx1ZXN5c3RlbS5jb20iLCJpYXQiOjE1OTIzNzYwOTcsImV4cCI6MTY4MjM3NjA5N30.9PFG_p3mJSJRWyCSej-AyMDsJnbC_0ZH8lHxv7rrYjZjTKQf3xf6rFCJVZEj-Oma9iQDupbVgU9VcDI37maOjA ", ParameterType.HttpHeader);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
            {
                var x = JArray.Parse(response.Content.ToString());

                datas = x.Select(p => new GetPositonsModel
                {
                    CentroX = (double)p["centro"][0],
                    CentroY = (double)p["centro"][1],
                    Inventario = (string)p["inventario"]

                }).ToList();

                return datas;

            }

            return datas;
        }

    }
}