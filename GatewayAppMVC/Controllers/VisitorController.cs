﻿using GatewayAppMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GatewayAppMVC.Controllers
{
    public class VisitorController : Controller
    {
        GatewayAppEntities db = new GatewayAppEntities();

        //Ziyaretçi kaydı get view 
        public ActionResult Index()
        {
            List<BeaconList> model = new List<BeaconList>();
            var list = db.Beacons.Where(x => x.IsEmpty == true).ToList();
            if (list != null)
            {
                foreach (var item in list)
                {
                    var beacon = new BeaconList();
                    beacon.Id = item.Id;
                    beacon.Name = "Cihaz" + " " + item.Id.ToString();

                    model.Add(beacon);
                }
            }

            return View(model);
        }

        //Ziyaretçi kaydetme
        public ActionResult SaveVisitor(AddVisitorModel model)
        {
            var visitorGateway = new List<VisitorGateway>();

            var visitor = new Visitor()
            {
                FullName = model.FullName,
                PhoneNumber = model.PhoneNumber,
                Address = model.Address,
                Email = model.Email,
            };
            db.Visitors.Add(visitor);
            var isSaved = db.SaveChanges();

            if (isSaved > 0)
            {
                var visitorBeacon = new VisitorBeacon()
                {
                    BeaconID = model.BeaconId,
                    VisitorID = visitor.Id,
                };
                db.VisitorBeacons.Add(visitorBeacon);
                var isSavedBeacon = db.SaveChanges();

                var beacon = db.Beacons.Where(x => x.Id == model.BeaconId).FirstOrDefault();
                if (beacon != null)
                {
                    beacon.IsEmpty = false;
                    db.SaveChanges();
                }


                if (isSavedBeacon > 0)
                {
                    var isAllowed1 = false;
                    var isAllowed2 = false;
                    var isAllowed3 = false;
                    var isAllowed4 = false;

                    //yönetime giriş yapabilecek(checBox checked)
                    if (model.Gateway1 != 0)
                    {
                        isAllowed1 = true;
                    }
                    //yönetime giriş yapamayacak(checBox unChecked)
                    else
                    {
                        var gt1 = db.Gateways.Where(x => x.Name.Contains("Gateway 1")).FirstOrDefault();
                        if (gt1 != null)
                        {
                            model.Gateway1 = gt1.Id;
                        }
                    }
                    var gateway1 = new VisitorGateway()
                    {
                        GatewayID = model.Gateway1,
                        VisitorID = visitor.Id,
                        IsAllowed = isAllowed1,
                    };

                    visitorGateway.Add(gateway1);

                    if (model.Gateway2 != 0)
                    {
                        isAllowed2 = true;
                    }
                    else
                    {
                        var gt2 = db.Gateways.Where(x => x.Name.Contains("Gateway 2")).FirstOrDefault();
                        if (gt2 != null)
                        {
                            model.Gateway2 = gt2.Id;
                        }
                    }
                    var gateway2 = new VisitorGateway()
                    {
                        GatewayID = model.Gateway2,
                        VisitorID = visitor.Id,
                        IsAllowed = isAllowed2,
                    };
                    visitorGateway.Add(gateway2);


                    if (model.Gateway3 != 0)
                    {
                        isAllowed3 = true;
                    }
                    else
                    {
                        var gt3 = db.Gateways.Where(x => x.Name.Contains("Gateway 3")).FirstOrDefault();
                        if (gt3 != null)
                        {
                            model.Gateway3 = gt3.Id;
                        }
                    }
                    var gateway3 = new VisitorGateway()
                    {
                        GatewayID = model.Gateway3,
                        VisitorID = visitor.Id,
                        IsAllowed = isAllowed3,
                    };
                    visitorGateway.Add(gateway3);


                    if (model.Gateway4 != 0)
                    {
                        isAllowed4 = true;
                    }
                    else
                    {
                        var gt4 = db.Gateways.Where(x => x.Name.Contains("Gateway 4")).FirstOrDefault();
                        if (gt4 != null)
                        {
                            model.Gateway4 = gt4.Id;
                        }
                    }
                    var gateway4 = new VisitorGateway()
                    {
                        GatewayID = model.Gateway4,
                        VisitorID = visitor.Id,
                        IsAllowed = isAllowed4,
                    };
                    visitorGateway.Add(gateway4);

                    db.VisitorGateways.AddRange(visitorGateway);
                    db.SaveChanges();

                }
            }

            List<BeaconList> modelBeacon = new List<BeaconList>();
            var list = db.Beacons.Where(x => x.IsEmpty == true).ToList();
            if (list != null)
            {
                foreach (var item in list)
                {
                    var beacon = new BeaconList();
                    beacon.Id = item.Id;
                    beacon.Name = item.Name;

                    modelBeacon.Add(beacon);
                }
            }

            ViewBag.Message = "Ziyaretçi kaydedildi";

            //return RedirectToAction("Index", modelBeacon);

            return View("Index", modelBeacon);

        }

        //Aktif  ziyaretçi listesi 
        public ActionResult ActiveVisitorList()
        {
            var activeBeacons = db.VisitorBeacons.Where(x => x.IsDeleted == false).Include(x => x.Visitor).ToList();


            var model = new List<ActiveVisitorListModel>();

            if (activeBeacons != null)
            {
                foreach (var item in activeBeacons)
                {
                    var visitor = new ActiveVisitorListModel();

                    visitor.AllowedGatewayList = new List<string>();
                    visitor.BannedGatewayList = new List<string>();

                    visitor.Id = item.Visitor.Id;
                    visitor.FullName = item.Visitor.FullName;
                    visitor.PhoneNumber = item.Visitor.PhoneNumber;
                    visitor.Address = item.Visitor.Address;
                    visitor.Email = item.Visitor.Email;

                    switch (item.Beacon.Name)
                    {
                        case "Herman Green":
                            visitor.BeaconName = "Cihaz 1";
                            break;
                        case "Anita Orange":
                            visitor.BeaconName = "Cihaz 2";
                            break;
                        case "Susan White":
                            visitor.BeaconName = "Cihaz 3";
                            break;
                        case "Bill Red":
                            visitor.BeaconName = "Cihaz 4";
                            break;
                        case "Peter Grey":
                            visitor.BeaconName = "Cihaz 5";
                            break;
                    }

                    var gatewayListForVisitor = db.VisitorGateways.Where(x => x.VisitorID == visitor.Id && x.IsDeleted == false).ToList();

                    foreach (var gt in gatewayListForVisitor)
                    {
                        if (gt.IsAllowed == true)
                        {

                            switch (gt.Gateway.Name)
                            {
                                case "Gateway 1":
                                    visitor.AllowedGatewayList.Add("Yönetim");
                                    break;
                                case "Gateway 2":
                                    visitor.AllowedGatewayList.Add("Danışma");
                                    break;
                                case "Gateway 3":
                                    visitor.AllowedGatewayList.Add("Ofis");
                                    break;
                                case "Gateway 4":
                                    visitor.AllowedGatewayList.Add("Teknik");
                                    break;
                                default:
                                    break;
                            }
       
                        }
                        else
                        {
                            switch (gt.Gateway.Name)
                            {
                                case "Gateway 1":
                                    visitor.BannedGatewayList.Add("Yönetim");
                                    break;
                                case "Gateway 2":
                                    visitor.BannedGatewayList.Add("Danışma");
                                    break;
                                case "Gateway 3":
                                    visitor.BannedGatewayList.Add("Ofis");
                                    break;
                                case "Gateway 4":
                                    visitor.BannedGatewayList.Add("Teknik");
                                    break;
                                default:
                                    break;
                            }

                        }
                    }

                    model.Add(visitor);
                }
            }

            return View(model);
        }

        //Ziyaretçiyi pasife çekme,silme(Kartı teslim alma)
        [HttpPost]
        public JsonResult PassiveVisitor(string id)
        {
            //visitorId
            var intId = Convert.ToInt32(id);
            //Kullanıcıya tanımlanan beacon bulunuyor
            var findBeacon = db.VisitorBeacons.Where(x => x.VisitorID == intId && x.IsDeleted == false).Include(x => x.Beacon).FirstOrDefault();
            if (findBeacon != null)
            {
                //Kullanıcıya tanımlanan beacon kaydı siliniyor
                findBeacon.IsDeleted = true;
                //Beacon boşa çekiliyor
                findBeacon.Beacon.IsEmpty = true;
            }
            //Kulanıcının yasaklı,izinli gateway listesi getiriliyor
            var gatewayList = db.VisitorGateways.Where(x => x.VisitorID == intId && x.IsDeleted == false).ToList();
            foreach (var item in gatewayList)
            {
                //Kulanıcının yasaklı,izinli gateway listesi siliniyor
                item.IsDeleted = true;
            }
            var isSaved = db.SaveChanges();
            if (isSaved > 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

    }
}